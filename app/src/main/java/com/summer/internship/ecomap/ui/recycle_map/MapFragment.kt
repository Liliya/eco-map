package com.summer.internship.ecomap.ui.recycle_map

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.summer.internship.ecomap.R
import com.summer.internship.ecomap.recycle_types.RecycleTypesView
import com.summer.internship.ecomap.recycle_types.RecycleTypesViewModel


class MapFragment: Fragment(), OnMapReadyCallback,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private lateinit var mapViewModel: MapViewModel
    private var mapView: View? = null

    private var googleApiClient: GoogleApiClient? = null
    private lateinit var map: GoogleMap
    private var locationRequest: LocationRequest? = null
    private lateinit var locationCallback: LocationCallback

    private var isGoogleApiConnected: Boolean = false
    private var isPermissionGranted: Boolean = false
    private var isGoogleMapReady: Boolean = false

    private val FINE_LOCATION_PERMISSION_REQUEST_CODE = 1
    private val CONNECTION_RESOLUTION_REQUEST_CODE = 2

    private val recycleTypesViewModel by lazy { ViewModelProviders.of(this).get(
        RecycleTypesViewModel::class.java)}

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        Log.d("Tag", "\nMapsActivity: onCreate()")
        super.onCreate(savedInstanceState)

        mapViewModel =
                ViewModelProviders.of(this).get(MapViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_map, container, false)
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        mapView = mapFragment?.view

        val fab:FloatingActionButton = root.findViewById(R.id.fab)


        val recycleTypesView: RecycleTypesView =  root.findViewById(R.id.recycle_types_view)
        recycleTypesViewModel.recycleTypesList.observe(viewLifecycleOwner, Observer {
            recycleTypesView.adapter.refreshRecycleTypes(it)
        })
        recycleTypesView.isVisible = false

        fab.setOnClickListener { view ->
            recycleTypesView.isVisible = !recycleTypesView.isVisible
        }

        buildGoogleAPIClient()
        return root
    }

    override fun onDestroy() {
        Log.d("Tag", "\nMapsActivity: onDestroy()")
        super.onDestroy()
    }

    override fun onResume() {
        Log.d("Tag", "\nMapsActivity: onResume()")
        super.onResume()
        buildGoogleAPIClient()

    }

    private fun buildGoogleAPIClient() {
        Log.d("Tag", "\nMapsActivity: buildGoogleAPIClient()")
        if (googleApiClient == null) {
            googleApiClient = this.context?.let {
                GoogleApiClient.Builder(it)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build()
            }
        }
        Log.d("Tag", "googleApiClient == null: ${googleApiClient == null}")
    }

    override fun onPause() {
        super.onPause()
        // here should be smth
        Log.d("Tag", "\nMapsActivity: onPause()")
    }

    override fun onStart() {
        Log.d("Tag", "\nMapsActivity: onStart()")
        super.onStart()
        googleApiClient?.connect()
        Log.d("Tag", "connect(), googleApiClient?.isConnected:  ${googleApiClient?.isConnected}")
    }

    override fun onStop() {
        Log.d("Tag", "\nMapsActivity: onStop()")
        googleApiClient?.disconnect()
        Log.d("Tag", "disconnect(), googleApiClient?.isConnected:  ${googleApiClient?.isConnected}")
        super.onStop()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Log.d("Tag", "\nMapsActivity: onMapReady()")
        map = googleMap
        isGoogleMapReady = true

        checkPermissionToAccessExactLocation()

        val mapSettings = map.uiSettings
        mapSettings?.isMyLocationButtonEnabled = true
        mapSettings?.isZoomControlsEnabled = true

        moveZoomControls(mapView, 30,-1,-1,90,false,false)

        getLastKnownLocationIfReady()
    }

    private fun checkPermissionToAccessExactLocation() {
        Log.d("Tag", "\nMapsActivity: checkPermissionToAccessExactLocation()")
        if (this.context?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) }
            == PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true
            isPermissionGranted = true
            Log.d("Tag", "permission granted")
        } else {
            // Request permission, cause it's missing
            Log.d("Tag", "need permission, do request")
            if (this.activity == null) {
                Log.d("Tag", "this.activity == null")
            }
            this.activity?.let {
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    FINE_LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    private fun getLastKnownLocationIfReady() {
        Log.d("Tag", "\nMapsActivity: getLastKnownLocationIfReady()")
        requestLocationAccess()
        if (this.context?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) }
            == PackageManager.PERMISSION_GRANTED
            && isGoogleApiConnected && isGoogleMapReady) {
            Log.d("Tag", "4 if = ok, ready to get last location")
            val fusedLocationClient: FusedLocationProviderClient? =
                this.activity?.let { LocationServices.getFusedLocationProviderClient(it) }
            fusedLocationClient?.lastLocation
                ?.addOnSuccessListener { location: Location? ->
                    // Got last known location. In some situations it can be null.
                    if (location != null) {
                        moveToLocation(location)
                    } else {
                        Log.d("Tag", "location == null, do request")
                        requestSingleLocationUpdate(fusedLocationClient)
                    }
                }
            if (fusedLocationClient == null) {
                Log.d("Tag", "fusedLocationClient == null")
            }
        } else {
            Log.d("Tag", "permission granted: ${this.context?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) }
                    == PackageManager.PERMISSION_GRANTED}, isApiConnected: $isGoogleApiConnected, isMapReady: $isGoogleMapReady")
        }
    }

    private fun requestLocationAccess() {
        Log.d("Tag", "\nMapsActivity: requestLocationAccess()")
        createLocationRequest()
        val builder: LocationSettingsRequest.Builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest!!)
        builder.setAlwaysShow(true) //this is the key ingredient
        val task: Task<LocationSettingsResponse>? =
            this.activity?.let { LocationServices.getSettingsClient(it).checkLocationSettings(builder.build()) }
        if (task == null) {
            Log.d("Tag", "task == null")
        }
        if (this.activity == null) {
            Log.d("Tag", "this.activity == null")
        }
        task?.addOnCompleteListener { task ->
            try {
                val response = task.getResult<ApiException>(ApiException::class.java)
                // Location settings are satisfied. The client can initialize location requests here
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied, but could be fixed by showing the
                        // user a dialog.
                        try {
                            Log.d("Tag", "catch: RESOLUTION_REQUIRED")
                            val resolvable: ResolvableApiException =
                                exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(this.activity, 2001)
                        }
                        catch (e: SendIntentException) {}
                        catch (e: ClassCastException) {}
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        Log.d("Tag", "catch: SETTINGS_CHANGE_UNAVAILABLE")
                    }
                }
            }
        }
    }

    private fun createLocationRequest() {
        if(locationRequest == null) {
            locationRequest = LocationRequest.create()
            locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest?.interval = 20 * 1000
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("Tag", "\nMapsActivity: onActivityResult()")
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CONNECTION_RESOLUTION_REQUEST_CODE
            && resultCode == Activity.RESULT_OK) {
            Log.d("Tag", "connect_resolution_request = result_ok")
            googleApiClient?.connect()
            Log.d("Tag", "googleApiClient connected: ${googleApiClient?.isConnected}")
        }
    }

    private fun moveToLocation(location: Location) {
        Log.d("Tag", "\nMapsActivity: moveToLocation()")
        val currentLatitude: Double = location.latitude
        val currentLongitude: Double = location.longitude
        map.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    currentLatitude,
                    currentLongitude
                ), 10f
            )
        )
        map.animateCamera(CameraUpdateFactory.zoomTo(16f), 2000, null)
    }

    private fun requestSingleLocationUpdate(fusedLocationClient: FusedLocationProviderClient?) {
        Log.d("Tag", "\nMapsActivity: requestSingleLocationUpdate()")
        if (this.context?.let {ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION)}
            == PackageManager.PERMISSION_GRANTED) {
            Log.d("Tag", "permission checked = ok")
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (loc in locationResult.locations){
                        moveToLocation(loc)
                        Log.d("Tag", "camera moved to current loc after single loc update")
                    }
                }
            }
            createLocationRequest()
            fusedLocationClient?.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null /* Looper */
                // cause of null it isn't necessary to stop location updates
                // it saves battery power, continuous location updates are't needed yet
                // if they are needed, it'll be necessary to start them in onResume()
                // and stop in onPause()
            )
            if (fusedLocationClient == null) {
                Log.d("Tag", "fusedLocationClient == null")
            } else {
                Log.d("Tag", "fusedLocationClient: location update requested")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        Log.d("Tag", "\nMapsActivity: onRequestPermissionsResult()")
        when (requestCode) {
            FINE_LOCATION_PERMISSION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            this.context?.let {
                                ContextCompat.checkSelfPermission(
                                    it,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                            }
                            == PackageManager.PERMISSION_GRANTED)) {
                    // Permission is granted. Continue workflow in the app.
                    // Enabling myLocation requires direct check with checkSelfPermission
                    map.isMyLocationEnabled = true
                    isPermissionGranted = true
                    getLastKnownLocationIfReady()
                    Log.d("Tag", "permission granted")
                } else {
                    isPermissionGranted = false
                    moveToSPbMoscowArea()
                    // Explain what features are unavailable because of a denied permission.
                    Toast.makeText(this.context, "Unable to show location - permission required",
                        Toast.LENGTH_LONG).show()
                    Log.d("Tag", "need permission")
                    Log.d("Tag", "this.context == null: ${this.context == null}")
                }
                return
            }
            // Here could be other 'when' lines to check for other permissions for the app.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun moveToSPbMoscowArea() {
        val spbMoscow = LatLng(58.048141, 33.414407)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(spbMoscow, 5.5f))
        map.animateCamera(CameraUpdateFactory.zoomTo(5.5f), 2000, null)
        Log.d("Tag", "camera moved to SpbMoscow")
    }

    override fun onConnected(p0: Bundle?) {
        Log.d("Tag", "\nMapsActivity: onConnected()")
        isGoogleApiConnected = true
        getLastKnownLocationIfReady()
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d("Tag", "\nMapsActivity: onConnectionSuspended()")
        Toast.makeText(this.context, "Connection suspended", Toast.LENGTH_SHORT).show()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.d("Tag", "\nMapsActivity: onConnectionFailed()")
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this.activity, CONNECTION_RESOLUTION_REQUEST_CODE)
            }
            catch (e: SendIntentException) {
                // There was an error with the resolution intent. Try again.
                googleApiClient?.connect()
            }
        } else {
            val dialog: Dialog = GoogleApiAvailability.getInstance()
                .getErrorDialog(this.activity, connectionResult.errorCode,
                    CONNECTION_RESOLUTION_REQUEST_CODE) // ??? here was 1
            dialog.show()
            isGoogleApiConnected = false
        }
    }
}
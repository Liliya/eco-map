package com.summer.internship.ecomap.recycle_types

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.summer.internship.ecomap.R


open class RecycleTypesView @JvmOverloads constructor(
    context:Context,
    attrs:AttributeSet? = null,
    defStyleAttr: Int = 0
): LinearLayout(context, attrs, defStyleAttr) {

    private var recyclerView: RecyclerView
    lateinit var adapter: RecycleTypesAdapter

    init {
        inflate(context,
            R.layout.view_recycle_types, this)
        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        adapter = RecycleTypesAdapter(context)
        recyclerView.adapter = adapter
    }


}


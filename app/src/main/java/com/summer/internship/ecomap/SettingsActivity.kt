package com.summer.internship.ecomap

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat

class SettingsActivity : AppCompatActivity() {

    private val email: String = "ecorecycle.map@gmail.com"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addFeedbackButton()
    }

    private fun addFeedbackButton() {
        val btnSendFeedback: Button = findViewById(R.id.btn_send_feedback)
        btnSendFeedback.setOnClickListener{
            val uriText:String = "mailto:" + Uri.encode(email) +
                    "?subject=" + Uri.encode(getString(R.string.feedback))
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.parse(uriText)
            )

            if (emailIntent.resolveActivity(packageManager) != null) {
                startActivity(Intent.createChooser(emailIntent, resources.getString(R.string.choose_mail_app)))
            } else {
                Toast.makeText(this, R.string.no_mail_app, Toast.LENGTH_LONG).show()
            }
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }
    }
}
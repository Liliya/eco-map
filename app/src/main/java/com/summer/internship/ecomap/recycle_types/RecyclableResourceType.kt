package com.summer.internship.ecomap.recycle_types

import com.summer.internship.ecomap.R

enum class RecyclableResourceType(val designationId: Int, val icId: Int) {
    PAPER(
        R.string.res1,
        R.drawable.res1
    ),
    TETRAPAK(
        R.string.res2,
        R.drawable.res2
    ),
    METAL(
        R.string.res3,
        R.drawable.res3
    ),
    GLASS(
        R.string.res4,
        R.drawable.res4
    ),
    BATTERY(
        R.string.res5,
        R.drawable.res5
    ),
    PLASTIC(
        R.string.res6,
        R.drawable.res6
    ),
    CLOTHES(
        R.string.res7,
        R.drawable.res7
    ),
    TOXIC(
        R.string.res8,
        R.drawable.res8
    ),
    LIDS(
        R.string.res9,
        R.drawable.res9
    ),
    APPLIANCES(
        R.string.res10,
        R.drawable.res10
    ),
    LAMPS(
        R.string.res11,
        R.drawable.res11
    ),
    OTHER(
        R.string.res12,
        R.drawable.res12
    )
}
package com.summer.internship.ecomap.recycle_types

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.common.util.ArrayUtils

class RecycleTypesViewModel: ViewModel() {

    var recycleTypesList : MutableLiveData<List<RecycleTypeItem>> = MutableLiveData()

    init {
        val recycleValues = ArrayUtils.toArrayList(RecyclableResourceType.values())
        recycleValues?.remove(RecyclableResourceType.PLASTIC)
        val recycleItems = ArrayList<RecycleTypeItem>(recycleValues.size )
        for (item in recycleValues) {
            recycleItems.add(
            RecycleTypeItem(
                item.designationId,
                item.icId
            ))
        }
        for (item in PlasticType.values()) {
            recycleItems.add(
            RecycleTypeItem(
                item.designationId,
                item.icId
            ))
        }
        recycleTypesList.value = recycleItems
    }

}
package com.summer.internship.ecomap.recycle_types

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.summer.internship.ecomap.R

class RecycleTypesAdapter(private val context: Context): RecyclerView.Adapter<RecycleTypesAdapter.ViewHolder>() {

    var items: List<RecycleTypeItem> = ArrayList()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView =
            itemView.findViewById(R.id.recycle_type_text)
        val imageView: ImageView = itemView.findViewById(R.id.recycle_type_image)
        val item = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recycleTypeView = inflater.inflate(R.layout.view_recycle_type_item, parent, false)
        return ViewHolder(
            recycleTypeView
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        var item = items[position]

        viewHolder.textView.text = context.getString(item.stringId)
        viewHolder.imageView.setImageDrawable(context.resources.getDrawable(item.drawableId))
        if (item.isSelected()) {
            viewHolder.item.setBackgroundResource(R.color.color_icon_selected)
        } else {
            viewHolder.item.setBackgroundResource(R.color.color_icon_background)
        }

        viewHolder.item.setOnClickListener(View.OnClickListener {
            item.setSelected(!item.isSelected())
            if (item.isSelected()) {
                viewHolder.item.setBackgroundResource(R.color.color_icon_selected)
            } else {
                viewHolder.item.setBackgroundResource(R.color.color_icon_background)
            }
            //TODO: add listener here
        })
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun refreshRecycleTypes(items: List<RecycleTypeItem>) {
        this.items = items
        notifyDataSetChanged()
    }

}
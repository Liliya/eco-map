package com.summer.internship.ecomap.recycle_types

import com.summer.internship.ecomap.R

enum class PlasticType(val designationId: Int, val icId: Int, val recyclable: Boolean) {
    PET_1(
        R.string.plastic1,
        R.drawable.plastic1, true),
    HDPE_2(
        R.string.plastic2,
        R.drawable.plastic2, true),
    PVC_3(
        R.string.plastic3,
        R.drawable.plastic3, false),
    LDPE_4(
        R.string.plastic4,
        R.drawable.plastic4, true),
    PP_5_SOFT(
        R.string.plastic5_1,
        R.drawable.plastic5,true),
    PP_5_HARD(
        R.string.plastic5_2,
        R.drawable.plastic5,true),
    PS_6(
        R.string.plastic6,
        R.drawable.plastic6, true),
    OTHER_7(
        R.string.plastic7,
        R.drawable.plastic7, false)
}
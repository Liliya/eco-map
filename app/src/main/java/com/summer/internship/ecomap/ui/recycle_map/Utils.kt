package com.summer.internship.ecomap.ui.recycle_map

import android.util.Log
import android.view.View
import android.widget.RelativeLayout


fun moveZoomControls(mapView: View?, left: Int, top: Int, right: Int, bottom: Int,
                             horizontal: Boolean, vertical: Boolean) {
    val zoomIn = mapView?.findViewWithTag<View>("GoogleMapZoomInButton")

    // we need the parent view of the zoomin/zoomout buttons - it didn't have a tag
    // so we must get the parent reference of one of the zoom buttons
    val zoomInOut = zoomIn?.parent as View?
    zoomInOut?.let { moveView(it, left, top, right, bottom, horizontal, vertical) }
}

/**
 * Move the View according to the passed params.  A -1 means to skip that one.
 *
 * NOTE:  this expects the view to be inside a RelativeLayout.
 *
 * @param view - a valid view
 * @param left - the distance from the left side
 * @param top - the distance from the top
 * @param right - the distance from the right side
 * @param bottom - the distance from the bottom
 * @param horizontal - boolean, center horizontally if true
 * @param vertical - boolean, center vertically if true
 */
fun moveView(view: View?, left: Int, top: Int, right: Int, bottom: Int,
                     horizontal: Boolean, vertical: Boolean) {
    try {
        // replace existing layout params
        val rlp: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        if (left >= 0) {
            rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE)//START
            rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE)
        }
        if (top >= 0) {
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
        }
        if (right >= 0) {
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE)//END
            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE)
        }
        if (bottom >= 0) {
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        }
        if (horizontal) {
            rlp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE)
        }
        if (vertical) {
            rlp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE)
        }
        rlp.setMargins(left, top, right, bottom)
        view?.layoutParams = rlp
    } catch (ex: Exception) {
        Log.e("Tag", "moveView() - failed: " + ex.localizedMessage)
        ex.printStackTrace()
    }
}

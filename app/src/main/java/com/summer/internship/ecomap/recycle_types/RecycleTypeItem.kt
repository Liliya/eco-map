package com.summer.internship.ecomap.recycle_types

data class RecycleTypeItem(val stringId: Int, val drawableId: Int) {
    private var isSelected: Boolean = false

    fun setSelected(selected: Boolean) {
        isSelected = selected
    }

    fun isSelected(): Boolean {
        return isSelected
    }
}
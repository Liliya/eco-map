package com.summer.internship.ecomap.ui.recycle_map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MapViewModel : ViewModel() {

    private val textString = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = textString
}
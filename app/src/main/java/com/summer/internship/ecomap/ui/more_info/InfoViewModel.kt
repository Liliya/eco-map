package com.summer.internship.ecomap.ui.more_info

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InfoViewModel : ViewModel() {

    private val textString = MutableLiveData<String>().apply {
        value = "This is info Fragment"
    }
    val text: LiveData<String> = textString
}